DOCKER=docker

all: build_and_run

stop_and_rm:
	if $(DOCKER) ps -a | grep -q airplane-spotter-app; \
	then $(DOCKER) stop airplane-spotter-app && $(DOCKER) rm airplane-spotter-app; \
	fi

build_and_run: stop_and_rm
	$(DOCKER) build -t airplane-spotter . && \
	  $(DOCKER) run -dit --name airplane-spotter-app -p 80:8080 -e MODULE_NAME=app \
	  -e VARIABLE_NAME=app \
	    -e AZURE_CUSTOM_VISION_ENDPOINT=${AZURE_CUSTOM_VISION_ENDPOINT} \
	      -e AZURE_CUSTOM_VISION_PREDICTION_KEY=${AZURE_CUSTOM_VISION_PREDICTION_KEY} \
	        -e AZURE_CUSTOM_VISION_PROJECT_ID=${AZURE_CUSTOM_VISION_PROJECT_ID} \
	          -e AZURE_CLIENT_ID=${AIRPLANE_SPOTTER_CLIENT_ID} \
	            -e AZURE_TENANT_ID=${ARM_TENANT_ID} \
	              -e AZURE_CLIENT_SECRET=${AIRPLANE_SPOTTER_CLIENT_SECRET} airplane-spotter

build:
	$(DOCKER) build -t airplane-spotter .
