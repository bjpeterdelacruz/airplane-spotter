import os
import requests


def test_upload_endpoint_missing_api_key():
    response = requests.post(f"{os.environ['AIRPLANE_SPOTTER_URL']}/upload")
    assert response.status_code == 400
    assert "API key is missing or does not match" in response.text

def test_upload_endpoint_missing_photograph():
    data = {'api-key': os.environ['AIRPLANE_SPOTTER_API_KEY']}
    response = requests.post(f"{os.environ['AIRPLANE_SPOTTER_URL']}/upload", data=data)
    assert response.status_code == 400
    assert "Missing photograph of airplane" in response.text

def test_upload_endpoint_boeing_747():
    files = {'photograph': open('../../united-787.jpg','rb')}
    data = {'api-key': os.environ['AIRPLANE_SPOTTER_API_KEY']}
    response = requests.post(f"{os.environ['AIRPLANE_SPOTTER_URL']}/upload", files=files, data=data)
    assert response.status_code == 200
    assert response.json()['aircraft_type'] == 'a Boeing 787 Dreamliner'
    assert response.json()['confidence_level'] == '96.8%'

def test_upload_endpoint_airbus_a350():
    files = {'photograph': open('../../singapore-airlines-a350.jpg','rb')}
    data = {'api-key': os.environ['AIRPLANE_SPOTTER_API_KEY']}
    response = requests.post(f"{os.environ['AIRPLANE_SPOTTER_URL']}/upload", files=files, data=data)
    assert response.status_code == 200
    assert response.json()['aircraft_type'] == 'an Airbus A350'
    assert response.json()['confidence_level'] == '99.5%'