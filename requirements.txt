azure-cognitiveservices-vision-customvision==3.1.1
azure-identity==1.20.0
azure-keyvault-secrets==4.9.0
Flask==3.1.0
matplotlib==3.10.1
msrest==0.7.1
waitress==3.0.2