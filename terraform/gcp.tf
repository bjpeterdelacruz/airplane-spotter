terraform {
  backend "s3" {
    bucket = "dev-bjdelacruz-terraform"
    key    = "airplane-spotter/state.tfstate"
    region = "us-east-2"
  }
}

provider "google" {
  project = var.GOOGLE_CLOUD_PROJECT_ID
  region  = "us-west1"
}

variable "GOOGLE_CLOUD_PROJECT_ID" {
  type = string
}

variable "AIRPLANE_SPOTTER_CLIENT_ID" {
  type = string
}

variable "AIRPLANE_SPOTTER_CLIENT_SECRET" {
  type      = string
  sensitive = true
}

variable "AZURE_CUSTOM_VISION_ENDPOINT" {
  type = string
}

variable "AZURE_CUSTOM_VISION_PREDICTION_KEY" {
  type      = string
  sensitive = true
}

variable "AZURE_CUSTOM_VISION_PROJECT_ID" {
  type = string
}

variable "AZURE_TENANT_ID" {
  type = string
}

resource "google_cloud_run_service" "airplane_spotter" {
  name     = "airplane-spotter"
  location = "us-west1"

  template {
    spec {
      containers {
        image = "bjdelacruz/airplane-spotter:latest"

        resources {
          limits = {
            memory = "256Mi"
            cpu    = "1"
          }
        }

        env {
          name  = "AZURE_CLIENT_ID"
          value = var.AIRPLANE_SPOTTER_CLIENT_ID
        }

        env {
          name  = "AZURE_CLIENT_SECRET"
          value = var.AIRPLANE_SPOTTER_CLIENT_SECRET
        }

        env {
          name  = "AZURE_CUSTOM_VISION_ENDPOINT"
          value = var.AZURE_CUSTOM_VISION_ENDPOINT
        }

        env {
          name  = "AZURE_CUSTOM_VISION_PREDICTION_KEY"
          value = var.AZURE_CUSTOM_VISION_PREDICTION_KEY
        }

        env {
          name  = "AZURE_CUSTOM_VISION_PROJECT_ID"
          value = var.AZURE_CUSTOM_VISION_PROJECT_ID
        }

        env {
          name  = "AZURE_TENANT_ID"
          value = var.AZURE_TENANT_ID
        }

        env {
          name  = "MODULE_NAME"
          value = "app"
        }

        env {
          name  = "VARIABLE_NAME"
          value = "app"
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service_iam_policy" "public_access" {
  location = google_cloud_run_service.airplane_spotter.location
  service  = google_cloud_run_service.airplane_spotter.name

  policy_data = <<EOF
{
  "bindings": [
    {
      "role": "roles/run.invoker",
      "members": [
        "allUsers"
      ]
    }
  ]
}
EOF
}

output "cloud_run_service_url" {
  description = "The URL of the deployed Cloud Run service"
  value       = google_cloud_run_service.airplane_spotter.status[0].url
}
