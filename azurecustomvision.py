from azure.cognitiveservices.vision.customvision.prediction import CustomVisionPredictionClient
from msrest.authentication import ApiKeyCredentials
from os import getenv

endpoint = getenv("AZURE_CUSTOM_VISION_ENDPOINT")
prediction_key = getenv("AZURE_CUSTOM_VISION_PREDICTION_KEY")

prediction_credentials = ApiKeyCredentials(in_headers={"Prediction-key": prediction_key})
predictor = CustomVisionPredictionClient(endpoint, prediction_credentials)

project_id = getenv("AZURE_CUSTOM_VISION_PROJECT_ID")


def classify_image(image_data):
    return predictor.classify_image(project_id=project_id, published_name="1st Iteration",
                                    image_data=image_data)
