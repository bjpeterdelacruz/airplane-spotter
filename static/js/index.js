'use strict';

if (location.protocol !== 'https:' && location.href !== 'http://127.0.0.1:5000/') {
    location.replace(`https:${location.href.substring(location.protocol.length)}`);
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function displayImage(input) {
    if (input.files && input.files[0]) {
        const img = document.getElementById("selected-image");
        img.setAttribute('style', 'display: none');
        document.getElementById('bar-chart').setAttribute('style', 'display: none');
        document.getElementById('message').setAttribute('style', 'display: none');

        if (input.files[0].size > 4194304) {
            alert(`File size is ${numberWithCommas(input.files[0].size)} bytes. Files must not be larger than ${numberWithCommas(4194304)} bytes.`);
            uploadButton.setAttribute('disabled', 'true');
            return;
        }
        const reader = new FileReader();

        reader.onload = (e) => {
            img.setAttribute("src", e.target.result);
            img.removeAttribute('style');
        };

        reader.readAsDataURL(input.files[0]);
        updateUploadButtonState();
    }
}

function updateUploadButtonState() {
    const uploadButton = document.getElementById('upload-button');
    const fileChooser = document.getElementById("photograph");
    const apiKeyField = document.getElementById("api-key");
    if (fileChooser.files && fileChooser.files[0] && apiKeyField.value) {
        uploadButton.removeAttribute('disabled');
    }
    else {
        uploadButton.setAttribute('disabled', true);
    }
}

async function sendImage() {
    const input = document.getElementById("photograph");
    const formData = new FormData();
    formData.append('photograph', input.files[0]);
    formData.append('api-key', document.getElementById("api-key").value)
    document.getElementById('message').setAttribute('style', 'display: none');
    document.getElementById('error').setAttribute('style', 'display: none');
    const response = await fetch("/upload", { method: 'POST', body: formData});
    if (!response.ok) {
        document.getElementById('error').removeAttribute('style');
        let text = await response.text();
        if (text.includes("<p>")) {
            text = text.substring(text.indexOf("<p>")).replace("<p>", "").replace("</p>", "");
        }
        document.getElementById('error').innerHTML = text;
        return;
    }
    const json = await response.json();
    const barChart = document.getElementById('bar-chart');
    barChart.setAttribute('src', json['bar_chart']);
    barChart.removeAttribute('style');
    document.getElementById('aircraft-type').textContent = json['aircraft_type'];
    document.getElementById('confidence-level').textContent = json['confidence_level'];
    document.getElementById('message').removeAttribute('style');
}

document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("copyright-year").innerHTML = new Date().getFullYear().toString();
    document.getElementById('upload-button').setAttribute('disabled', 'true');
    document.getElementById('api-key').addEventListener('input', updateUploadButtonState);
});
