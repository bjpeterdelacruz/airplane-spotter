FROM python:3.13.2
LABEL maintainer="bj.peter.delacruz@gmail.com"
LABEL version="1.0"

RUN pip install --upgrade pip

COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY static /app/static
COPY templates /app/templates
COPY *.py /app/

WORKDIR /app
CMD ["python", "wsgi.py"]