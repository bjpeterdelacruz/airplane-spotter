# Airplane Spotter

This Flask web application uses Azure Custom Vision to determine whether an aircraft in a photograph
that was uploaded by a user is either a Boeing 787 Dreamliner or an Airbus A350.

A live demo is hosted on Cloud Run in Google Cloud:

- [https://airplane-spotter-knartlettq-uw.a.run.app](https://airplane-spotter-knartlettq-uw.a.run.app)

![Airbus A350](https://bjdelacruz.dev/files/airplanespotter-airbus.png)

![Boeing 787 Dreamliner](https://bjdelacruz.dev/files/airplanespotter-boeing.png)

#### Profiles

- [Developer's Website](https://bjdelacruz.dev)
- [Coderbyte](https://coderbyte.com/profile/bjpeterdelacruz)
- [Codesignal](https://app.codesignal.com/profile/bjpeter)
- [Codewars](https://www.codewars.com/users/bjpeterdelacruz/stats)
