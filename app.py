from flask import Flask, render_template, request, abort
import azurecustomvision
from base64 import b64encode
import matplotlib
import matplotlib.pyplot as plt
import io
from azurekeyvault import api_key

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/upload', methods=['POST'])
def upload():
    if api_key != request.form.get("api-key"):
        print("Invalid or missing API key!")
        abort(400, description="API key is missing or does not match")
    matplotlib.pyplot.switch_backend('Agg')  # https://stackoverflow.com/a/66567690/3640269
    if 'photograph' not in request.files:
        print("Missing photograph of airplane!")
        abort(400, description="Missing photograph of airplane")
    file = request.files['photograph']
    data = file.read()
    results = azurecustomvision.classify_image(data)
    predictions = {prediction.tag_name: prediction.probability
                   for prediction in results.predictions}
    tags = {'airbus-a350': 'Airbus A350', 'boeing-787': 'Boeing 787 Dreamliner'}
    aircraft_types = []
    probabilities = []
    for tag_name, probability in predictions.items():
        aircraft_types.append(tags[tag_name])
        probabilities.append(probability * 100)
    plt.bar([0, 1], probabilities, color='green')
    plt.xlabel('Aircraft Type')
    plt.ylabel('Percent Confidence Level')
    plt.title('Likelihood of Aircraft in Photograph')
    plt.xticks([0, 1], aircraft_types)
    io_bytes = io.BytesIO()
    plt.savefig(io_bytes, format='jpg')
    io_bytes.seek(0)
    bar_chart = f"data:image/jpeg;base64,{b64encode(io_bytes.read()).decode('utf-8')}"
    if predictions['boeing-787'] > predictions['airbus-a350']:
        aircraft_type = "a Boeing 787 Dreamliner"
        confidence_level = "{:.1f}%".format(predictions['boeing-787'] * 100)
    else:
        aircraft_type = "an Airbus A350"
        confidence_level = "{:.1f}%".format(predictions['airbus-a350'] * 100)
    print("Aircraft Type:", aircraft_type)
    print("Confidence Level:", confidence_level)
    return {'bar_chart': bar_chart, 'aircraft_type': aircraft_type,
            'confidence_level': confidence_level}
